#!/bin/bash

# REGISTRY=194.67.90.137:5000
# SERVICE_NAME=test-nginx
# PUBLISH_PORT=15000
# BIND_PORT=80

docker stop ${SERVICE_NAME}
docker pull ${REGISTRY}/${SERVICE_NAME}
docker run -itd --rm --name ${SERVICE_NAME} -p 0.0.0.0:${PUBLISH_PORT}:${BIND_PORT}  ${REGISTRY}/${SERVICE_NAME}
